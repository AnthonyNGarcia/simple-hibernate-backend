import React, { useEffect } from 'react';
import apiBasePath from '../apiBasePath';
import axios from 'axios';
import { connect } from 'react-redux';
import './styling.css';

function EmployeeTableComponent(props) {
    /**
         * This component must stay up to date so it will fetch from the API via the getCodes method
         * The method was separated out for cleaner, easier to understand code with minimal nesting
         */
    useEffect(() => {
        getEmployees();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    /**
     * Fetch all the codes from the API
     */
    async function getEmployees() {
        const res = await axios({
            method: 'GET',
            url: apiBasePath + '/all',
        });
        console.log(res.data);
        props.setAllEmployees(res.data);
        console.log(props.allEmployees);
    }

    const viewEmployee = (props, targetId) => {
        // By selecting to view an employee, we cancel any updating
        props.setUpdating(false);
        // First we need to grab the employee by their Id
        let allEmployees = [...props.allEmployees];

        let currentEmployee = allEmployees.filter(employee => employee.employeeId === targetId)[0];

        //now we set the current employee to view
        props.viewEmployee(currentEmployee);
    }

    return (
        <React.Fragment>
            <div className="container tableContainer">
                <div className="row">
                    <div className="col-sm">
                    </div>
                    <div className="col-sm">
                    </div>
                    <div className="col-sm">
                    </div>
                </div>
                <div className="row">
                    <div className="col-12">
                        <table className="table">
                            <thead>
                                <tr>
                                    <th scope="col-8" className="white">Employee Name</th>
                                    <th scope="col-4" className="white">Job Level</th>
                                </tr>
                            </thead>
                            <tbody>
                                {(props.allEmployees.length <= 0) ? <tr><th className="white">The directory has no employees to view.</th></tr> : props.allEmployees.map((employee) =>
                                    <tr key={employee.employeeId}>
                                        <td><button onClick={viewEmployee.bind(null, props, employee.employeeId)} type="button" className="btn btn-primary all-codes-btn">{employee.firstName + " " + employee.lastName}</button></td>
                                        <td className="white">{employee.jobLevel}</td>
                                    </tr>
                                )}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </React.Fragment>

    )
}

function mapStateToProps(state) {
    return { ...state }
}

function mapDispatchToProps(dispatch) {
    return {
        setAllEmployees: (employeeList) => dispatch({
            type: 'SET_ALL_EMPLOYEES',
            employeeList: employeeList
        }),
        viewEmployee: (employee) => dispatch({
            type: 'VIEW_EMPLOYEE',
            employee: employee
        }),
        setUpdating: (isUpdating) => dispatch({
            type: 'SET_UPDATING',
            isUpdating: isUpdating
        })

    }
}

export default connect(mapStateToProps, mapDispatchToProps)(EmployeeTableComponent);