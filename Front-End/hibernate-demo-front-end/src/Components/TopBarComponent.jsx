import React from 'react';
import './styling.css';

function TopBarComponent() {
    return (
        <React.Fragment>
            <div className="topBarContainer">
                <h3 className="topBarTitle">Employee Directory</h3>
            </div>
        </React.Fragment>

    )
}

export default TopBarComponent;