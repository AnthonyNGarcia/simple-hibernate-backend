import React, { useState } from 'react';
import apiBasePath from '../apiBasePath';
import axios from 'axios';
import { connect } from 'react-redux';
import './styling.css';

function ViewEmployeeComponent(props) {

    // Function state for form input
    const [jobLevel, setJobLevel] = useState("");
    const [phoneNumber, setPhoneNumber] = useState("");
    const [email, setEmail] = useState("");

    // Update form for user input as they type
    const changeJobLevel = (e) => {
        setJobLevel(e.target.value);
    }

    const changePhoneNumber = (e) => {
        setPhoneNumber(e.target.value);
    }

    const changeEmail = (e) => {
        setEmail(e.target.value);
    }

    const startUpdateEmployee = (props) => {
        // Change state to reflect that we are now updating employee
        setJobLevel(props.currentEmployee.jobLevel);
        setEmail(props.currentEmployee.email);
        setPhoneNumber(props.currentEmployee.phoneNumber);
        props.setUpdating(true);
    }

    function submitUpdate(e) {
        e.preventDefault();
        finishUpdateEmployee(props, jobLevel, email, phoneNumber);
    }

    const finishUpdateEmployee = async (props, jobLevel, email, phoneNumber) => {
        const updatedEmployee = {
            employeeId: props.currentEmployee.employeeId,
            firstName: props.currentEmployee.firstName,
            lastName: props.currentEmployee.lastName,
            jobLevel: jobLevel,
            email: email,
            phoneNumber: phoneNumber
        }

        const res = await axios({
            method: 'PUT',
            url: apiBasePath + '/update',
            data: updatedEmployee
        });
        console.log(res.data);
        if (res.status === 400) {
            alert(res.data.errorMessage);
            return;
        } else if (res.status !== 200) {
            console.log("Error updating, not a bad request?");
            return;
        } else {
            // Now let's update the redux store
            let allEmployees = [...props.allEmployees];

            let remainingEmployees = allEmployees.filter(employee => employee.employeeId !== updatedEmployee.employeeId);
            remainingEmployees.push(updatedEmployee);
            props.setAllEmployees(remainingEmployees);
            props.viewEmployee(updatedEmployee);
            props.setUpdating(false);
        }
    }

    const cancelUpdateEmployee = (e) => {
        e.preventDefault();
        props.setUpdating(false);
    }

    const deleteEmployee = async (props) => {
        let targetId = props.currentEmployee.employeeId;

        const res = await axios({
            method: 'DELETE',
            url: apiBasePath + '/delete?employeeId=' + targetId,
        });
        console.log(res.data);
        // Let's now remove this employee from our redux store
        let allEmployees = [...props.allEmployees];

        let remainingEmployees = allEmployees.filter(employee => employee.employeeId !== targetId);
        props.setAllEmployees(remainingEmployees);
        props.setUpdating(false);
        props.viewEmployee(null);
    }

    if (props.currentEmployee === null) {
        return (
            <React.Fragment>
                <div className="employeeContainer">
                    <h1 className="white">Select an Employee to view more details.</h1>
                </div>
            </React.Fragment>
        )
    } else {
        if (props.isUpdating === true) {
            return (
                <React.Fragment>
                    <div className="employeeContainer">
                        <h1 className="employeeName">{props.currentEmployee.firstName + " " + props.currentEmployee.lastName}</h1>
                        <form onSubmit={submitUpdate}>
                            <div className="form-group">
                                <label for="jobLevelInput">Job Level</label>
                                <input onChange={changeJobLevel} className="input-field" type="number" id="jobLevelInput" value={jobLevel} placeholder="Job Level (1-10)" required />
                                <br />
                                <label for="emailInput">Work Email</label>
                                <input onChange={changeEmail} className="input-field" type="email" id="emailInput" value={email} placeholder="Work Email ( @infosys.com )" required />
                                <br />
                                <label for="phoneNumberInput">Work Phone Number</label>
                                <input onChange={changePhoneNumber} className="input-field" type="number" id="phoneNumberInput" value={phoneNumber} placeholder="11 digit phone number ( 12345678901 )" required />
                            </div>
                            <button type="submit" className="btn btn-primary" onClick={submitUpdate}>Submit Changes</button>
                            <button type="button" className="btn btn-danger" onClick={cancelUpdateEmployee}>Cancel Changes</button>
                        </form>
                    </div>
                </React.Fragment>
            )

        } else {
            return (
                <React.Fragment>
                    <div className="employeeContainer">
                        <h1 className="employeeName">{props.currentEmployee.firstName + " " + props.currentEmployee.lastName}</h1>
                        <h4 className="employeeField">{"Job Level: " + props.currentEmployee.jobLevel}</h4>
                        <h4 className="employeeField">{"Work Email: " + props.currentEmployee.email}</h4>
                        <h4 className="employeeField">{"Work Telephone: " + props.currentEmployee.phoneNumber}</h4>
                        <button type="button" className="btn btn-primary" onClick={startUpdateEmployee.bind(null, props)}>Update Information</button>
                        <button type="button" className="btn btn-danger" onClick={deleteEmployee.bind(null, props)}>Delete Employee</button>
                    </div>
                </React.Fragment>
            )
        }
    }
}

function mapStateToProps(state) {
    return { ...state }
}

function mapDispatchToProps(dispatch) {
    return {
        setAllEmployees: (employeeList) => dispatch({
            type: 'SET_ALL_EMPLOYEES',
            employeeList: employeeList
        }),
        viewEmployee: (employee) => dispatch({
            type: 'VIEW_EMPLOYEE',
            employee: employee
        }),
        setUpdating: (isUpdating) => dispatch({
            type: 'SET_UPDATING',
            isUpdating: isUpdating
        })
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ViewEmployeeComponent);