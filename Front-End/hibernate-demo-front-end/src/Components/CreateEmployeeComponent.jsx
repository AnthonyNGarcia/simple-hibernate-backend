import React, { useState } from 'react';
import apiBasePath from '../apiBasePath';
import axios from 'axios';
import { connect } from 'react-redux';
import './styling.css';

function CreateEmployeeComponent(props) {

    // Function state for form input
    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [jobLevel, setJobLevel] = useState("");
    const [phoneNumber, setPhoneNumber] = useState("");
    const [email, setEmail] = useState("");

    // Update form for user input as they type
    const changeFirstName = (e) => {
        setFirstName(e.target.value);
    }

    const changeLastName = (e) => {
        setLastName(e.target.value);
    }

    const changeJobLevel = (e) => {
        setJobLevel(e.target.value);
    }

    const changePhoneNumber = (e) => {
        setPhoneNumber(e.target.value);
    }

    const changeEmail = (e) => {
        setEmail(e.target.value);
    }

    async function submitEmployee(e) {
        e.preventDefault();
        const newEmployee = {
            firstName: firstName,
            lastName: lastName,
            jobLevel: jobLevel,
            email: email,
            phoneNumber: phoneNumber
        }

        const res = await axios({
            method: 'POST',
            url: apiBasePath + '/add',
            data: newEmployee
        });
        console.log(res.data);
        if (res.status === 400) {
            alert(res.data.errorMessage);
            return;
        } else if (res.status !== 200) {
            console.log("Error creating, not a bad request?");
            return;
        } else {
            // Now let's update the redux store
            let allEmployees = [...props.allEmployees];

            newEmployee.employeeId = res.data.employeeId;

            allEmployees.push(newEmployee);
            props.setAllEmployees(allEmployees);

            // And clear the forms
            setFirstName("");
            setLastName("");
            setJobLevel("");
            setEmail("");
            setPhoneNumber("");
        }
    }

    return (
        <React.Fragment>
            <div className="createContainer">
                <h5 className="employeeName">Add New Employee</h5>
                <form onSubmit={submitEmployee}>
                    <div className="form-group">
                        <label for="firstNameInput">First Name</label>
                        <input onChange={changeFirstName} className="input-field" type="text" id="firstNameInput" value={firstName} placeholder="First Name (cannot change later)" required />
                        <br />

                        <label for="lastNameInput">Last Name</label>
                        <input onChange={changeLastName} className="input-field" type="text" id="lastNameInput" value={lastName} placeholder="Last Name (cannot change later)" required />
                        <br />

                        <label for="jobLevelInput">Job Level</label>
                        <input onChange={changeJobLevel} className="input-field" type="number" id="jobLevelInput" value={jobLevel} placeholder="Job Level (1-10)" required />
                        <br />
                        <label for="emailInput">Work Email</label>
                        <input onChange={changeEmail} className="input-field" type="email" id="emailInput" value={email} placeholder="Work Email ( @infosys.com )" required />
                        <br />
                        <label for="phoneNumberInput">Work Phone Number</label>
                        <input onChange={changePhoneNumber} className="input-field" type="number" id="phoneNumberInput" value={phoneNumber} placeholder="11 digit phone number ( 12345678901 )" required />
                    </div>
                    <button type="submit" className="btn btn-success" onClick={submitEmployee}>Add to Directory</button>
                </form>
            </div>
        </React.Fragment>
    )
}

function mapStateToProps(state) {
    return { ...state }
}

function mapDispatchToProps(dispatch) {
    return {
        setAllEmployees: (employeeList) => dispatch({
            type: 'SET_ALL_EMPLOYEES',
            employeeList: employeeList
        })
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CreateEmployeeComponent);