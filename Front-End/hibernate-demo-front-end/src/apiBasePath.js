/**
 * The api base path exported here is used to connect to the back-end api
 */

 const apiBasePath = 'http://localhost:9090';

 export default apiBasePath;