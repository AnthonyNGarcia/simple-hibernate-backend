/**
 * This file stores our reducer logic
 */

const initialState = {
    allEmployees: [],
    currentEmployee: null,
    isUpdating: false
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case 'VIEW_EMPLOYEE':
            return {
                ...state,
                currentEmployee: action.employee
            }
        case 'SET_ALL_EMPLOYEES':
            console.log("setting all employees...");
            console.log(action.employeeList);
            return {
                ...state,
                allEmployees: [...action.employeeList]
            }
        case 'SET_UPDATING':
            return {
                ...state,
                isUpdating: action.isUpdating
            }
        default:
            return state;
    }
}

export default reducer;
