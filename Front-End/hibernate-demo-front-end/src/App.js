import React from 'react';
import CreateEmployeeComponent from './Components/CreateEmployeeComponent';
import EmployeeTableComponent from './Components/EmployeeTableComponent';
import './Components/styling.css';
import TopBarComponent from './Components/TopBarComponent';
import ViewEmployeeComponent from './Components/ViewEmployeeComponent';

function App() {
  return (
    <div className="appContainer">
      <TopBarComponent />
      
      <EmployeeTableComponent />
      
      <div className="container">
        <div className="row">
          <div className="col">
          <ViewEmployeeComponent />
          </div>
          <div className="col">
          <CreateEmployeeComponent />
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
