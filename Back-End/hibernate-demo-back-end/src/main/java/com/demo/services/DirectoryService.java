package com.demo.services;

import java.util.List;

import com.demo.models.Employee;

/**
 * Interface for planning out what the directory service layer should offer
 * @author anthony.garcia01
 *
 */
public interface DirectoryService {
	
	Employee addEmployee(Employee employee);
	Employee getEmployeeById(Integer employeeId);
	Employee updateEmployee(Employee updatedEmployee);
	Employee deleteEmployee(Employee employee);
	List<Employee> getAllEmployees();

}
