package com.demo.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.models.Employee;
import com.demo.models.EmployeeErrorMessage;
import com.demo.repositories.DirectoryRepository;

/**
 * ~~Service Layer~~
 * This is the implementation of the DirectoryService interface, which will
 * handle validation and calls to the repository layer.
 * @author anthony.garcia01
 *
 */
@Service
public class DirectoryServiceImpl implements DirectoryService {
	
	private DirectoryRepository directoryRepository;
	
	@Autowired
	public DirectoryServiceImpl(DirectoryRepository directoryRepository) {
		super();
		this.directoryRepository = directoryRepository;
	}

	/**
	 * The employee object expected must be completely filled out, except for employeeId
	 * The successfully added employee object with an Id will be returned to the user
	 */
	@Override
	public Employee addEmployee(Employee employee) {
		// 1. Confirm valid employee information
		// 2. Add employee to database
		String errorMessage = validateEmployee(employee);
		
		if (errorMessage.isEmpty()) {
			return this.directoryRepository.addEmployee(employee);
		} else {
			return new EmployeeErrorMessage(errorMessage);
		}
	}
	
	/**
	 * An employee can have any of its fields updated as long as they are valid, except Id
	 * The successfully updated employee object, with Id, is returned to the user
	 */
	@Override
	public Employee updateEmployee(Employee updatedEmployee) {
		// 1. Confirm valid employee information
		// 2. Update employee in database
		String errorMessage = validateEmployee(updatedEmployee);
		
		if (errorMessage.isEmpty()) {
			return this.directoryRepository.updateEmployee(updatedEmployee);
		} else {
			return new EmployeeErrorMessage(errorMessage);
		}
	}

	/**
	 * Simple and straightforward- provide an employeeId, get an Employee object
	 */
	@Override
	public Employee getEmployeeById(Integer employeeId) {
		if (employeeId==null) {
			return null;
		}
		return this.directoryRepository.getEmployeeById(employeeId);
	}

	/**
	 * The user provides an employee object that they want to delete, and it
	 * is removed from the database. This provided employee object must contain
	 * the employeeId for a successful delete to occur.
	 */
	@Override
	public Employee deleteEmployee(Employee employee) {
		try {
			return this.directoryRepository.deleteEmployee(employee.getEmployeeId());			
		} catch (IllegalArgumentException e) {
			return null;
		}
	}

	/**
	 * No input, a simple return of a list of all employees currently in the system.
	 */
	@Override
	public List<Employee> getAllEmployees() {
		return this.directoryRepository.getAllEmployees();
	}
	
	/**
	 * A utility method to modularize employee validation to meet certain parameters
	 * @param employee the data which is to be validated
	 * @return String errorMessage either empty or with an errorMessage to report
	 */
	private String validateEmployee(Employee employee) {
		// VALIDATION
		// 1. Phone number is exactly 11 digits.
		// 2. Job level is between (1,10) inclusive
		// 3. Email ends with @infosys.com (no personal emails allowed)
		// 4. Return the error message, empty or not
		
		// 1. Phone number is exactly 10 digits.
		String phoneNumber = "" + employee.getPhoneNumber();
		String errorMessage = "";
		if (phoneNumber.length() !=11 ) {
			errorMessage = "Invalid phone number provided. Please provide an 11 digit phone number.";
		}
		
		// 2. Job level is between (1,10) inclusive
		int jobLevel = employee.getJobLevel();
		if (jobLevel < 1 || jobLevel > 10) {
			errorMessage = "Invalid job level provided. Please provide a job level between 1 and 10, inclusive.";
		}
		
		// 3. Email ends with @infosys.com (no personal emails allowed)
		String email = employee.getEmail();
		if (!email.endsWith("@infosys.com")) {
			errorMessage = "Invalid email provided. Please provide a valid format Infosys work email.";
		}
		
		// 4. Return the error message, empty or not
		return errorMessage;
	}

}
