package com.demo.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.demo.models.Employee;
import com.demo.models.EmployeeErrorMessage;
import com.demo.services.DirectoryService;

/**
 * ~~Controller Layer~~
 * This is the controller containing all endpoints for this employee directory API
 * It features the standard CRUD operations, including getAll (5 endpoints)
 * @author anthony.garcia01
 *
 */
@RestController
@CrossOrigin(origins="*")
public class DirectoryController {
	
	private DirectoryService directoryService;
	
	
	@Autowired
	public DirectoryController(DirectoryService directoryService) {
		super();
		this.directoryService = directoryService;
	}

	@GetMapping("/hello")
	public static ResponseEntity<String> helloWorld(){
		System.out.println("User hit helloWorld endpoint!");
		return new ResponseEntity<>("Hello World!", HttpStatus.OK);
	}
	
	@GetMapping("/all")
	public ResponseEntity<List<Employee>> getAllEmployees() {
		List<Employee> allEmployees = directoryService.getAllEmployees();
		if (allEmployees == null || allEmployees.size() == 0) {
			return new ResponseEntity<>(null, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(allEmployees, HttpStatus.OK);
		}
	}
	
	@GetMapping("/getById")
	public ResponseEntity<Employee> getEmployeeById(@RequestParam int employeeId) {
		Employee existingEmployee = directoryService.getEmployeeById(employeeId);
		if (existingEmployee == null) {
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		} else {
			return new ResponseEntity<>(existingEmployee, HttpStatus.OK);
		}
	}
	
	@PostMapping("/add")
	public ResponseEntity<Employee> addEmployee(@RequestBody Employee employee) {
		System.out.println(employee.toString());
		Employee existingEmployee = directoryService.getEmployeeById(employee.getEmployeeId());
		if (existingEmployee != null) {
			System.out.println("Found employee already.");
			System.out.println(existingEmployee.toString());
		} else {
			System.out.println("Employee doesn't currently exist in database. Proceeding to attempt to add it.");
		}
		if (existingEmployee == null) {
			try {
				System.out.println("About to add employee");
				Employee newEmployee = directoryService.addEmployee(employee);
				System.out.println("Added employee. Proceeding to validate and return");
				return validateEmployee(newEmployee);
			} catch (Exception e) {
				System.out.println("Error adding new employee: ");
				System.out.println(employee.toString());
				e.printStackTrace();
				return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
			}
		} else {
			System.out.println("User tried to add an employee that already exists by Id.");
			System.out.println(employee.toString());
			return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
		}
	}
	
	@PutMapping("/update")
	public ResponseEntity<Employee> updateEmployee(@RequestBody Employee updatedEmployee) {
		Employee existingEmployee = directoryService.getEmployeeById(updatedEmployee.getEmployeeId());
		if (existingEmployee == null) {
			System.out.println("User tried to update an employee that doesn't exist, or " +
					" is trying to update an employee's id");
			updatedEmployee.toString();
			return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
		} else {
			try {
				Employee newEmployee = directoryService.updateEmployee(updatedEmployee);
				// Must check if this newEmployee we were returned has any error message
				return validateEmployee(newEmployee);
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println("Failed to update employee.");
				updatedEmployee.toString();
				return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}
	}
	
	@DeleteMapping("/delete")
	public ResponseEntity<Employee> deleteEmployee(@RequestParam int employeeId) {
		try {
			Employee deletingEmployee = directoryService.deleteEmployee(new Employee(employeeId));
			if (deletingEmployee == null) {
				return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
			} else {
				return new ResponseEntity<>(deletingEmployee, HttpStatus.OK);				
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("There was an issue deleting employee by Id.");
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	private ResponseEntity<Employee> validateEmployee(Employee newEmployee) {
		// Check if the employee object we received is actually an EmployeeErrorMessage
		if (newEmployee instanceof EmployeeErrorMessage) {
			return new ResponseEntity<>(newEmployee, HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<>(newEmployee, HttpStatus.OK);
		}
	}
}
