package com.demo.repositories;

import java.util.List;

import com.demo.models.Employee;

/**
 * Interface for planning out what the directory repository layer should offer
 * @author anthony.garcia01
 *
 */
public interface DirectoryRepository {
	
	Employee addEmployee(Employee employee);
	Employee getEmployeeById(Integer employeeId);
	Employee updateEmployee(Employee updatedEmployee);
	Employee deleteEmployee(Integer employeeId);
	List<Employee> getAllEmployees();

}
