package com.demo.repositories;

import java.util.List;

import javax.persistence.EntityManagerFactory;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.demo.models.Employee;

/**
 * ~~Repository/DAO Layer~~ This is the implementation of the
 * DirectoryRepository interface, which assumes valid input and simply does the
 * direct calls to the database
 * 
 * @author anthony.garcia01
 *
 */
@Transactional
@Repository
public class DirectoryRepositoryImpl implements DirectoryRepository {

	private SessionFactory sesFact;

	@Autowired
	public DirectoryRepositoryImpl(EntityManagerFactory factory) {
		super();
		if (factory.unwrap(SessionFactory.class) == null) {
			throw new NullPointerException("Factory is not a hibernate factory!");
		}
		this.sesFact = factory.unwrap(SessionFactory.class);
	}

	@Override
	public Employee addEmployee(Employee employee) {
		Session session = sesFact.openSession();
		Employee employeeWrapperObject = new Employee((Integer) session.save(employee));
		session.close();
		return employeeWrapperObject;
	}

	@Override
	public Employee deleteEmployee(Integer employeeId) throws IllegalArgumentException{
		Session session = sesFact.openSession();
		
		session.beginTransaction();

		Employee deletingEmployee = session.get(Employee.class, employeeId);

		session.delete(deletingEmployee);
		
		session.getTransaction().commit();
		
		session.close();

		return deletingEmployee;
	}

	@Override
	public Employee updateEmployee(Employee employee) {
		Session session = sesFact.openSession();

		Employee updatedEmployee = session.get(Employee.class, employee.getEmployeeId());

		updatedEmployee.setEmail(employee.getEmail());
		updatedEmployee.setFirstName(employee.getFirstName());
		updatedEmployee.setLastName(employee.getLastName());
		updatedEmployee.setJobLevel(employee.getJobLevel());
		updatedEmployee.setPhoneNumber(employee.getPhoneNumber());

		session.update(updatedEmployee);
		
		session.close();

		return updatedEmployee;
	}

	@Override
	public Employee getEmployeeById(Integer employeeId) {
		Session session = sesFact.openSession();

		Employee existingEmployee = session.get(Employee.class, employeeId);
		
		session.close();

		return existingEmployee;
	}

	@Override
	public List<Employee> getAllEmployees() {
		Session session = sesFact.openSession();

		List<Employee> allEmployees = session.createQuery("from Employee", Employee.class).list();
		
		session.close();

		return allEmployees;
	}

}
