package com.demo.models;

/**
 * This is a subclass of Employee, meaning it can be provided wherever Employee
 * is expected. Validation must occur when an EmployeeErrorMessage might be received,
 * and this validation occurs in the service layer. This is a way for a requested
 * operation to return a successful object, or an error message, while preserving
 * the advantage of compile-time safety with Java generics for method return types
 * @author anthony.garcia01
 *
 */
public class EmployeeErrorMessage extends Employee {
	
	private String errorMessage;

	public EmployeeErrorMessage(String errorMessage) {
		super();
		this.errorMessage = errorMessage;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	@Override
	public String toString() {
		return "EmployeeErrorMessage [errorMessage=" + errorMessage + "]";
	}
	

}
