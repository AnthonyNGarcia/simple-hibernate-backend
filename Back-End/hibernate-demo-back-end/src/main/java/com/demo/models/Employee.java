package com.demo.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * The employee is the object representation of data regarding an employee in the directory.
 * It has no special methods other than traditional constructors, getters/setters, and toString
 * @author anthony.garcia01
 *
 */
@Entity
@Table(name="employees")
public class Employee {
	
	
	/////// FIELDS /////////
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="employee_id")
	private Integer employeeId;
	
	@Column(name="first_name", nullable=false)
	private String firstName;
	
	@Column(name="last_name", nullable=false)
	private String lastName;
	
	@Column(name="email", nullable=false)
	private String email;
	
	@Column(name="phone_number", nullable=false)
	private Long phoneNumber;
	
	@Column(name="job_level", nullable=false)
	private Integer jobLevel;
	
	//////// CONSTRUCTORS ////////
	
	/**
	 * No-args constructor for new employee instantiation without immediate declaration of fields
	 */
	public Employee() {
		super();
	}


	/**
	 * This general purpose constructor is for a full Employee object when fetching from or updating a database entry
	 * @param firstName
	 * @param lastName
	 * @param email
	 * @param phoneNumber
	 * @param jobLevel
	 * @param employeeId
	 */
	public Employee(String firstName, String lastName, String email, Long phoneNumber, Integer jobLevel, Integer employeeId) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.phoneNumber = phoneNumber;
		this.jobLevel = jobLevel;
		this.employeeId = employeeId;
	}
	
	/**
	 * This general purpose constructor is for receiving an Employee object to submit new to a database (no ID)
	 * @param firstName
	 * @param lastName
	 * @param email
	 * @param phoneNumber
	 * @param jobLevel
	 */
	public Employee(String firstName, String lastName, String email, Long phoneNumber, Integer jobLevel) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.phoneNumber = phoneNumber;
		this.jobLevel = jobLevel;
	}
	
	/**
	 * This constructor acts as a wrapper for incoming JSON's where an employee ID is being received
	 * @param employeeId
	 */
	public Employee(Integer employeeId) {
		this.employeeId = employeeId;
	}

	
	///////// GETTERS, SETTERS AND TOSTRING ///////////

	

	public Integer getEmployeeId() {
		return employeeId;
	}


	public void setEmployeeId(Integer employeeId) {
		this.employeeId = employeeId;
	}


	public String getFirstName() {
		return firstName;
	}


	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}


	public String getLastName() {
		return lastName;
	}


	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public Long getPhoneNumber() {
		return phoneNumber;
	}


	public void setPhoneNumber(Long phoneNumber) {
		this.phoneNumber = phoneNumber;
	}


	public Integer getJobLevel() {
		return jobLevel;
	}


	public void setJobLevel(Integer jobLevel) {
		this.jobLevel = jobLevel;
	}


	@Override
	public String toString() {
		return "Employee [employeeId=" + employeeId + ", firstName=" + firstName + ", lastName=" + lastName + ", email="
				+ email + ", phoneNumber=" + phoneNumber + ", jobLevel=" + jobLevel + "]";
	}


}
