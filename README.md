# simple-hibernate-backend

Getting Started:
1. Launch Spring Boot application, easily through Spring Boot Dashboard if using SpringToolSuite (STS)
2. Launch React application; can do once navigated inside project folder and running "npm start"
3. Navigate to browser at http://localhost:3000 to view front-end and begin interacting with application

Demo project for a Spring Boot configured Java backend and
Hibernate for database interactions with an H2 database.

Utilized SpringORM to facilitate convenient configurations
and to leverage the bean management of the Spring IoC container.

